# Change logs v7.0.0


## Changes Log
    2022-10-10 v7.1.1: Add support all >= packages
    2020-08-20 v7.1.0: Remove Blueprint macros
    2020-08-19 v7.0.0: Upgrade media library