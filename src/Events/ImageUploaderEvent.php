<?php

namespace HalcyonLaravel\Base\Events;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

abstract class ImageUploaderEvent
{
    protected $model;

    protected $media;

    /**
     * @var string
     */
    protected $action;

    public function __construct(HasMedia $model, Media $media)
    {
        $this->model = $model;
        $this->media = $media;
    }

    public function getModel(): HasMedia
    {
        return $this->model;
    }

    public function getMedial(): Media
    {
        return $this->media;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

}